-module(csvdb).
-export([test/0, initialize_server/1]).

-import(file, [list_dir/1]).
-import(proplists, [lookup/2]).

-record(coldef, {name, type}).
-record(table, {name, rows, columns}).

%
%
% {select
%	[{from TableName}
%	,{where
%		{exp {eq 1 2}}
%	}
%	
%
%



initialize_server(TableList) ->
	receive
		{FromPiD, Action , Table, Attrs} ->
			case lookup(Table, TableList) of
				none -> {error, "Table not found!"};
				TableRec -> FromPiD ! dispatch_query(Action, TableRec, Attrs)
			end;
		_ ->
			io:format("Unknown format. Please go die in a fire~n",[])
	end.

dispatch_query(Action, TableRec, Attrs) ->
	case Action of
		select -> select(TableRec, Attrs);
		insert -> insert(TableRec, Attrs);
		delete -> delete(TableRec, Attrs);
		update -> update(TableRec, Attrs);
		_ -> ok
	end.

select(Rec, Attrs) ->
	ok.

insert(Rec, Attrs) ->
	ok.

delete(Rec, Attrs) ->
	ok.

update(Rec, Attrs) ->
	ok.

spawn_server() ->
	TableList = prefetch_tables("tables/").
	%spawn(csvdb, initialize_server, TableList).

prefetch_tables(Directory) ->
	case list_dir(Directory) of
		{ok, Names} ->
			TableList = prefetch_all_tables(Names),
			TableList;
		_ ->
			io:format("Error reading dir: ~s~n",[Directory]),
			{error, "Error reading dir"}
	end.

parse_rows(TableContent) ->
	Rows = lists:reverse(string:tokens(TableContent, "\n")),
	lists:foldl(fun(X, List) -> [lists:reverse(parse_columns(X)) | List] end, [], Rows).

parse_columns(Column) ->
	Columns = string:tokens(Column, ","),
	lists:foldl(fun(X, List) -> [parse_field(X) | List] end, [], Columns).

parse_field(Field) ->
	Field.

generate_table_record(TableName) ->
	All = prefetch_table(TableName),
	[Rows | Columns] = All,
	#table{name=TableName, rows=Rows, columns=Columns}.

prefetch_all_tables(Tables) ->
	TableContents = lists:foldl(fun(X, List) -> [generate_table_record(X) | List] end, [], Tables),
	TableContents.

prefetch_table(Table) ->
	case file:read_file(lists:append("tables/", Table)) of
		{ok, File} -> 
			Content = unicode:characters_to_list(File),
			parse_rows(Content);
		_ ->
			io:format("Error reading file", [])
	end.

test() ->
	spawn_server().
